package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.TaskDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.service.ConnectionService;
import ru.t1.dkononov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.dkononov.tm.constant.TestData.*;

@Category(DataCategory.class)
public class TaskRepositoryTest extends AbstractSchemaTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final TaskDTORepository repository = new TaskDTORepository(entityManager);

    @BeforeClass
    public static void before() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void init() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDTORepository repository = new TaskDTORepository(entityManager);
            @NotNull final UserDTORepository userDTORepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userDTORepository.add(USER1);
            repository.add(USER1.getId(), USER_TASK);
            repository.add(USER1.getId(), USER_TASK2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDTORepository repository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER_TASK);
            Assert.assertEquals(USER_TASK.getUserId(), USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDTORepository repository = new TaskDTORepository(entityManager);
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER_TASK);
            Assert.assertEquals(USER_TASK, repository.findById(USER_TASK.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findById() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDTORepository repository = new TaskDTORepository(entityManager);
            Assert.assertNotNull(repository.findById(USER1.getId(), USER_TASK.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final TaskDTORepository repository = new TaskDTORepository(entityManager);
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
