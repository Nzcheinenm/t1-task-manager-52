package ru.t1.dkononov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class ProjectSchemaTest extends AbstractSchemaTest {

    @Test
    public void Test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("schema,project");
    }

}
